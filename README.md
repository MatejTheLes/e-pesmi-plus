Pozdravljeni, profesor, če vidite ta readme.

Rad bi vas obvestil, da to ni moj prvi poskus pri izdelavi te domače naloge ampak se mi je pri prejšnjem (in ostalih treh, štirih, ne vem več koliko) pri merganju posameznih branchev zalomilo in končen izdelek ni deloval tako, kot bi mogel. (Tudi pri tokratnji implementaciji je tako, košarica račun ne deluje.) Zato sem rešitve skopiral na računalnik, še enkrat forkal vaš original in samo prilepil svoje rešitve ter pravilno mergal rezultate. Zato izgleda, kot da je bila moja naloga rešena v kakšni uri.

Hvala za razumevanje in lep pozdrav, Matej Leskovšek (63160196)